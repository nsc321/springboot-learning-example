package org.spring.springboot.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.annotation.JmsListeners;
import org.springframework.stereotype.Component;

/**
 * Created by NSC on 2017/11/6.
 * Never give up
 */

@Component
public class JMSConsumer {

    @JmsListeners({
            @JmsListener(destination = ActiveMQConfig.ACTIVEMQ_QUEUE_ONE),
            @JmsListener(destination = ActiveMQConfig.ACTIVEMQ_QUEUE_TWO)
    })
    private void listenMqQueue(String message){
        System.err.println("消费信息：" + message);
    }
}
