package org.spring.springboot.jms;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;

/**
 * Created by NSC on 2017/11/6.
 * Never give up
 */
@Configuration
public class ActiveMQConfig {

    public static final String ACTIVEMQ_QUEUE_ONE = "activemq_queue_one";

    public static final String ACTIVEMQ_QUEUE_TWO = "activemq_queue_two";

    @Bean
    public Queue oneQueue(){
        return new ActiveMQQueue(ACTIVEMQ_QUEUE_ONE);
    }

    @Bean
    public Queue twoQueue(){
        return new ActiveMQQueue(ACTIVEMQ_QUEUE_TWO);
    }
}
