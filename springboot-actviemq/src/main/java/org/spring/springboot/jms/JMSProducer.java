package org.spring.springboot.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

/**
 * Created by NSC on 2017/11/6.
 * Never give up
 */
@Component
public class JMSProducer {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue oneQueue;

    @Autowired
    private Queue twoQueue;

    public void sendMessageToOneMq(String message){
        jmsTemplate.convertAndSend(oneQueue,message);
    }

    public void sendMessageToTwoMq(String message){
        jmsTemplate.convertAndSend(twoQueue,message);
    }
}
