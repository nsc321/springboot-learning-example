package org.spring.springboot.controller;

import org.spring.springboot.jms.JMSProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by NSC on 2017/11/6.
 * Never give up
 */

@RestController
public class MessageController {

    @Autowired
    private JMSProducer jmsProducer;

    @RequestMapping("tomq")
    public void toMqMessage(String message){

        if(message == null || message.length() == 0)
            return ;

        System.err.println("消息：" + message);
        jmsProducer.sendMessageToOneMq("this is first one mq of :" + message);
        jmsProducer.sendMessageToTwoMq("this is second two mq of :"  + message);
    }
}
